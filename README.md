# SpaceXLaunch Programmer

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
### components
```
component folder consists two component one is card component and other is YearFilterList.
```
### view
```
Folder view contain the main part view inside Home Component,
```
### App.vue
```
App component render the Home
```
### style.css

```
style of an app contains inside assets folder in style.css file
```
### Gitlab Repo Url
```
Use gitbal for code keeping here is the repo url mentioned below

https://gitlab.com/shubhamjain283203/spacexlaunchprogramme.git

```
### Code Deployed At 

```
Code deployed at onrender here is the url mentioned below.
https://spaxlaunch.onrender.com/ 
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
